# 快速上手

> 基于 cesium 和 geowayglobe 快速创建三维地球

### 1. 引入 cesium

```js
<link rel="stylesheet" href="http://172.16.67.78/sdk/cesium-sdk/Cesium/Widgets/widgets.css">
<script src="http://172.16.67.78/sdk/cesium-sdk/Cesium/Cesium.js"></script>
```
> 下载地址：https://gitlab.com/webglobe/web-globe-sdk/-/tree/master/cesium-master

### 2. 引入 gw-terrain

```js
<script src="./marked.min.js"></script>
```

### 3. 新建 div 容器

```js
<div id="viewer"></div>
```

### 4. 使用 new Cesium.Viewer 方法创建地球

```js
 window.viewer = new Cesium.Viewer('viewer', { 
    animation: false,
    baseLayerPicker: false,
    fullscreenButton: false,
    geocoder: false,
    homeButton: false,
    sceneModePicker: false,
    selectionIndicator: false,
    shadows: false,
    timeline: false,
    navigationHelpButton:false,
    infoBox: false, 
    navigationInstructionsInitiallyVisible: false, 
    shouldAnimate: false, 
    terrainProvider: new GeowayGlobe.TiledTerrainProvider({ 
        connections: [{ 
            priority: 0,
            url: 'http://172.16.67.34:8066/ime-cloud/rest/globe_world_01_10_v2/terrain' + '/data?level={TileMatrix}&col={TileCol}&row={TileRow}', credit: new Cesium.Credit('WorldTerrain'), 
            rectangle: new Cesium.Rectangle.fromDegrees(-180, -90, 180, 90), minLevel: 1, maxLevel: 10, tileMatrixLabels: ['1', '2', '3', '4', '5', '6', '7','8','9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19'] 
        }] 
    })
}); 
```

### 5. 飞行到指定位置

```js
viewer.camera.flyTo({ destination: Cesium.Cartesian3.fromDegrees(101.60939085239706, 37.59462875407051, 8202), });
```

### 6. 地球成功创建
