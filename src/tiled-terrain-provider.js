/* eslint-disable */
import pako from './pako_inflate';

var julianDateScratch = new Cesium.JulianDate();

function TerrainCache() {
    this._terrainCache = {};
    this._lastTidy = Cesium.JulianDate.now();
}

TerrainCache.prototype.add = function(quadKey, buffer) {
    this._terrainCache[quadKey] = {
        buffer : buffer,
        timestamp : Cesium.JulianDate.now()
    };
};

TerrainCache.prototype.get = function(quadKey) {
    var terrainCache = this._terrainCache;
    var result = terrainCache[quadKey];
    if (Cesium.defined(result)) {
        delete this._terrainCache[quadKey];
        return result.buffer;
    }
};

TerrainCache.prototype.tidy = function() {
    Cesium.JulianDate.now(julianDateScratch);
    if (Cesium.JulianDate.secondsDifference(julianDateScratch, this._lastTidy) > 10) {
        var terrainCache = this._terrainCache;
        var keys = Object.keys(terrainCache);
        var count = keys.length;
        for (var i = 0; i < count; ++i) {
            var k = keys[i];
            var e = terrainCache[k];
            if (Cesium.JulianDate.secondsDifference(julianDateScratch, e.timestamp) > 10) {
                delete terrainCache[k];
            }
        }

        Cesium.JulianDate.clone(julianDateScratch, this._lastTidy);
    }
};

function TiledTerrainProvider(options) {
    this._connections = options.connections;
    this._tilingScheme = new Cesium.GeographicTilingScheme({
        numberOfLevelZeroTilesX : 2,
        numberOfLevelZeroTilesY : 1,
        ellipsoid : Cesium.Ellipsoid.WGS84
    });
    this._width = Cesium.defaultValue(options.tileCellNum, 33);
    this._height = Cesium.defaultValue(options.tileCellNum, 33);
    this._levelZeroMaximumGeometricError = Cesium.TerrainProvider.getEstimatedLevelZeroGeometricErrorForAHeightmap(this._tilingScheme.ellipsoid, 65, this._tilingScheme.getNumberOfXTilesAtLevel(0));

    this._connections.sort(function(m, n) {
        if (m.priority < n.priority) {
            return 1;
        } else if (m.priority > n.priority) {
            return -1;
        }
        return 0;
    });
    this._rectangle = this._connections[0].rectangle;
    if(Cesium.defined(this._connections[0].minLevel)){
        this._minLevel = this._connections[0].minLevel - 1;
    }else {
        this._minLevel =0;
    }
    this._maxLevel = this._connections[0].maxLevel - 1;
    this._resource = undefined;
    this._tilesAvailable = new Cesium.TileAvailability(this._tilingScheme, 14);
    this._tilesAvailable.addAvailableTileRange(0, 0, 0,
        this._tilingScheme.getNumberOfXTilesAtLevel(0), this._tilingScheme.getNumberOfYTilesAtLevel(0));
    var rectangle = new Cesium.Rectangle;
    var that = this;
    this._connections.forEach(function(provider) {
        provider.resource = Cesium.Resource.createIfNeeded(provider.url);
        provider.minLevel -= 1;
        if (that._minLevel > provider.minLevel) {
            that._minLevel = provider.minLevel;
        }
        provider.maxLevel -= 1;
        if (that._maxLevel < provider.maxLevel) {
            that._maxLevel = provider.maxLevel;
        }
        Cesium.Rectangle.expand(that._rectangle, provider.rectangle, rectangle);
        that._rectangle = rectangle;
    });
    this._ready = true;
    this._errorEvent = new Cesium.Event();
    this._readyPromise = Cesium.when.resolve(true);
    this._terrainCache = new TerrainCache();
}

Object.defineProperties(TiledTerrainProvider.prototype, {

    errorEvent : {
        get : function() {
            return this._errorEvent;
        }
    },

    credit : {
        get : function() {
            //>>includeStart('debug', pragmas.debug);
            if (!this.ready) {
                throw new Cesium.DeveloperError('credit must not be called before ready returns true.');
            }
            //>>includeEnd('debug');
            return this._credit;
        }
    },

    tilingScheme : {
        get : function() {
            //>>includeStart('debug', pragmas.debug);
            if (!this.ready) {
                throw new Cesium.DeveloperError('tilingScheme must not be called before ready returns true.');
            }
            //>>includeEnd('debug');
            return this._tilingScheme;
        }
    },

    ready : {
        get : function() {
            return this._ready;
        }
    },

    readyPromise : {
        get : function() {
            return this._readyPromise;
        }
    },

    hasWaterMask : {
        get : function() {
            return false;
        }
    },

    hasVertexNormals : {
        get : function() {
            return false;
        }
    },

    availability : {
        get : function() {
            return this._tilesAvailable;
        }
    }
});

TiledTerrainProvider.prototype.getLevelMaximumGeometricError = function(level) {
    //>>includeStart('debug', pragmas.debug);
    if (!this.ready) {
        throw new Cesium.DeveloperError('getLevelMaximumGeometricError must not be called before ready returns true.');
    }
    //>>includeEnd('debug');

    return this._levelZeroMaximumGeometricError / (1 << level);
};

var rectangleScratch = new Cesium.Rectangle();
var taskProcessor = null;

// 若用的是Geoway内部的cesium sdk，则用cesium worker策略
if (Cesium.TileServiceLayer) {
    taskProcessor = new Cesium.TaskProcessor('decodeGeowayTerrainPacket',10);
}

TiledTerrainProvider.prototype.requestTileGeometry = function(x, y, level, request) {
    if (!this._ready) {
        throw new Cesium.DeveloperError('requestTileGeometry must not be called before the terrain provider is ready.');
    }

    if (level<this._minLevel) {
        return new Cesium.HeightmapTerrainData({
            buffer : new Float32Array(33 * 33),
            width : 33,
            height : 33
        });
    }

    var hasData = true;
    var minLevel;
    var tileRectangle = this._tilingScheme.tileXYToRectangle(x, y, level, rectangleScratch);
    var insertRectangle;
    var that = this;
    var labels;
    this._connections.some(function(provider) {
        insertRectangle = Cesium.Rectangle.intersection(tileRectangle, provider.rectangle, rectangleScratch);
        if (Cesium.defined(insertRectangle)) {
            labels = provider.tileMatrixLabels;
            that._resource = provider.resource;
            minLevel = provider.minLevel;
            hasData = true;
            return true;
        }
        hasData = false;

    });

    if (Cesium.defined(minLevel) && level < minLevel) {
        hasData = false;
    }

    if (!hasData) {
        return new Cesium.HeightmapTerrainData({
            buffer : new Float32Array(33 * 33),
            width : 33,
            height : 33
        });
    }

    var tileMatrix = Cesium.defined(labels) ? labels[level] : level.toString();

    var tileResource;

    var templateValues = {
        TileMatrix : tileMatrix,
        TileRow : y.toString(),
        TileCol : x.toString()
    };

    this._resource.setTemplateValues(templateValues);

    tileResource = this._resource.getDerivedResource({
        request : request
    });

    var terrainCache = this._terrainCache;
    var quadKey = getTileCacheKey([x, y, level]);
    var buffer = terrainCache.get(quadKey);
    var promise,decodePromise,taskPromise;
    if (Cesium.defined(buffer)){
        if (Cesium.TileServiceLayer) {
            decodePromise = taskProcessor.scheduleTask({
                buffer : buffer,
                width : that._width
            }, [buffer]);
        } else {
            decodePromise = that.parseTerrainBuffer(buffer,  that._width, tileMatrix, x.toString(), y.toString());
        }
    }else{
        promise = tileResource.fetchArrayBuffer();
        if (!Cesium.defined(promise)) {
            return undefined;
        }

        decodePromise = promise.then(function(buffer) {
            if (Cesium.defined(buffer)) {
                if (Cesium.TileServiceLayer) {
                    taskPromise = taskProcessor.scheduleTask({
                        buffer : buffer,
                        width : that._width
                    }, [buffer]);
                } else {
                    taskPromise = that.parseTerrainBuffer(buffer,  that._width, tileMatrix, x.toString(), y.toString());
                }

                if(!Cesium.defined(taskPromise)){
                    terrainCache.add(quadKey, buffer);
                    return undefined;
                }
                    return taskPromise;

            }
        });
    }

    terrainCache.tidy();

    if (!Cesium.defined(decodePromise)) {
        return undefined;
    }

    return decodePromise
        .then(function(result) {
            if (result.buffer && isNaN(result.buffer[0])) {
                console.error('地形切片有错误，位置为：level=' + result.level + ', x=' + result.x + ', y=' + result.y);
                return new Cesium.HeightmapTerrainData({
                    buffer: new Float32Array(that._width * that._width),
                    width: that._width,
                    height: that._width
                });
            } else {
                return new Cesium.HeightmapTerrainData({
                    buffer : result.buffer,
                    width : that._width,
                    height : that._width
                });
            }
        })
        .otherwise(function(error) {
            var message = 'An error occurred while accessing ' + that._resource.url + '.';
            Cesium.TileProviderError.handleError(undefined, that, that._errorEvent, message);
            return Cesium.when.reject(error);
        }
    );
};

function getTileCacheKey(x, y, level) {
    return JSON.stringify([x, y, level]);
}

TiledTerrainProvider.prototype.parseTerrainBuffer = function(buffer, width, level, x, y) {
    var pos = 0;
    var view = new DataView(buffer);

    pos += Uint32Array.BYTES_PER_ELEMENT;

    var min_val = view.getFloat32(pos, true);
    pos += Float32Array.BYTES_PER_ELEMENT;

    var d_operator = view.getFloat64(pos, true);
    pos += Float64Array.BYTES_PER_ELEMENT;

    var zip_data = new Uint8Array(buffer, pos, buffer.byteLength - 16);
    // var blob = new Blob(buffer);
    var uncompressedPacket = pako.inflate(zip_data);
    var unzaip_data_size = width * width;
    var uncompressedbuffer = new Uint16Array(uncompressedPacket.buffer);
    var gridbuffer = new Float32Array(unzaip_data_size);//uncompressedPacket.buffer;
    var i = 0, j = 0;
    for (i = 0; i < unzaip_data_size; i++) {
        var idx = Math.ceil((width - 1) - i / width) * width + (i % width);
        var relative_height = uncompressedbuffer[i];
        if (relative_height < 0) {
            gridbuffer[idx] = relative_height;
        } else {
            gridbuffer[idx] = relative_height * d_operator + min_val;
        }
    }

    var gridbuffer1 = new Float32Array(unzaip_data_size);
    for (i = 0; i < width; i++) {
        for (j = 0; j < width; j++) {
            gridbuffer1[i * width + j] = gridbuffer[(width - 1 - i) * width + j];
        }
    }

    return Promise.resolve({buffer : gridbuffer1, level: level, x: x, y: y})
}

TiledTerrainProvider.prototype.getTileDataAvailable = function(x, y, level) {
    var result = isTileAvailable(this, level, x, y);
    if (Cesium.defined(result)) {
        return result;
    }

    return undefined;
};

function isTileAvailable(that, level, x, y) {
    var maxLevel;
    var tileRectangle = that._tilingScheme.tileXYToRectangle(x, y, level, rectangleScratch);
    that._connections.some(function(provider) {
        var insertRectangle = Cesium.Rectangle.intersection(tileRectangle, provider.rectangle, rectangleScratch);
        if (Cesium.defined(insertRectangle)) {
            maxLevel = provider.maxLevel;
            return true;
        }
    });

    if(!Cesium.defined(maxLevel)){
        return false;
    }

    if (level > maxLevel) {
        return false;
    }

    return true;
}

TiledTerrainProvider.prototype.loadTileDataAvailability = function(x, y, level) {
    return undefined;
};

export {TiledTerrainProvider};